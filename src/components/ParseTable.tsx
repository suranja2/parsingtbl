import { Grammar, NonterminalSymbol, TerminalSymbol, Epsilon, epsilon } from '../ll'

const htmlEps = String.fromCharCode(949)

interface Props {
	grammar: Grammar;
	parseTable: Map<NonterminalSymbol, Map<TerminalSymbol|Epsilon, Set<number>>>;
}

export default function ParsingTable({grammar, parseTable}: Props) {
    function copyPTasJSONToClipboard(event: React.MouseEvent<HTMLButtonElement>) {
        const parsingTableJSON: { [key: string]: { [key: string]: number } } = {};

        grammar.nonterminals.forEach(N => {
            const tableRow: { [key: string]: number } = {};

            [...grammar.terminals, htmlEps].forEach(T => {
                const entries = parseTable.get(N)?.get(T);
                if (entries && entries.size >= 0) {
                    entries.forEach((val: number) => {
                        tableRow[T] = val + 1;
                    });
                } else if (T === htmlEps) {
                    for (let i = 0; i < grammar.rules.length; i++) {
                        if (grammar.rules[i].lhs === N && grammar.rules[i].rhs.length === 0) {
                            tableRow[T] = i + 1;
                            break;
                        }
                    }
                }
            });
            parsingTableJSON[N] = tableRow;
        });

        const jsonString = JSON.stringify(parsingTableJSON, null, 2);

        navigator.clipboard.writeText(jsonString).then(() => {
            console.log("JSON copied to clipboard");
        }).catch((error) => {
            console.error("Failed to copy JSON to clipboard:", error);
            console.log(jsonString);
        })
    }

    return (
        <div>
            <table className="table table-bordered table-sm">
                <caption>LL1 parsing table</caption>
                <thead className="table-light">
                <tr>
                    <th></th>
                    {grammar.terminals.map((symb) => (<th><code>{symb}</code></th>))}
                    <th><code>{htmlEps}</code></th>
                </tr>
                </thead>
                <tbody>
                {grammar.nonterminals.map(N => (
                    <tr key={N}>
                        <th className="table-light"><code>{N}</code></th>
                        {[...grammar.terminals, epsilon].map(T => {
                            let entries = parseTable.get(N)?.get(T)
                            if (entries) {
                                return (
                                    <td className={entries.size > 1 ? "table-danger" : ""}>
                                        {[...entries].map((val: number) => val + 1).join(", ")}
                                    </td>
                                )
                            } else {
                                return (<td></td>)
                            }
                        })}
                    </tr>
                ))}
                </tbody>
            </table>
            <button type="button" className="btn btn-primary" onClick={copyPTasJSONToClipboard}>
                Copy as JSON to clipboard
            </button>
        </div>
    )
}
